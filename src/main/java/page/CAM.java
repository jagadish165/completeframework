package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class CAM extends Base{
    private By proof = By.id("proof");
    private By country =By.id("country");

//    public CAM(WebDriver driver) {
//        super(driver);
//    }

    public CAM selectProof(String proof) {
        WebElement proofEle = driver.findElement(this.proof);
        select.selectValue(proofEle,proof);
        return new CAM();
    }
    public CAM selectCountry(String country) {
        WebElement countryele = driver.findElement(this.country);
        select.selectValue(countryele,country);
        return new CAM();
    }
//    public String verifyheader(){
//        return driver.findElement(header).getText();
//    }

}
