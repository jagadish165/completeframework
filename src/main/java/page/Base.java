package page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import report.ExtentReport;
import webutil.PropertyUtil;
import wrapper.CustomWebDriver;
import wrapper.CustomSelect;
import wrapper.CustomWebElement;

import java.io.IOException;
import java.util.Properties;

public class Base {
    public static Logger logger;
    static CustomSelect select;
    static CustomWebElement webElement;
    static WebDriver driver ;
    public static ExtentReport extent;
    public static Properties properties ;
      volatile CustomWebDriver webDriverUtil;
    public void setup() throws IOException {
        logger.info("setup started");
        webDriverUtil = new CustomWebDriver();
        System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver.exe");
        CustomWebDriver.threadLocal.set(new ChromeDriver());
        driver = CustomWebDriver.threadLocal.get();
       // System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver.exe");
      //  driver = new ChromeDriver();
        webElement = new CustomWebElement(driver);
        select = new CustomSelect(driver);
        driver.manage().deleteAllCookies();
        System.out.println(driver.getWindowHandle());
        System.out.println(Thread.currentThread().getId());
        driver.get("http://localhost:90");
        logger.info("setup ended");
    }
    public void initiateReport(){
        extent = ExtentReport.getInstance();
        logger = LogManager.getLogger();
        properties = PropertyUtil.loadProperties("src/main/resources/properties/env.properties");
    }
    public void killDrivers(){
        Runtime rt = Runtime.getRuntime();
        try {
            rt.exec("taskkill /im chromedriver.exe /f /t");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void tearDown(){
        driver.quit();
       CustomWebDriver.threadLocal.remove();
        driver = null;

    }

    public void closeReport() {
        extent.extentReports.flush();
    }
}
