package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerSearch extends Base {

        private By searchType = By.id("search");
        private By textBox = By.id("enter");
        private By submit = By.xpath("//input[@value='Submit']");

//        public CustomerSearch(WebDriver driver){
//            super(driver);
//        }


        public CustomerSearch selectSearchType(String searchType){
            WebElement searchTypeEle = driver.findElement(this.searchType);
            select.selectValue(searchTypeEle,searchType);
            return this;
        }
        public CustomerSearch enterValue(String value){
           webElement.sendKeys(textBox,value);
        //   driver.findElement(textBox).sendKeys(value);
           // select.selectValue(role,);
            return this;
        }
        public CustomerProfile submit(){
            webElement.click(submit);
           // driver.findElement(submit).click();
            return new CustomerProfile();
        }

    }


