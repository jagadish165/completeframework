package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerProfile extends Base{

        private By CAM = By.linkText("CAM");
        private By OLB =By.linkText("OLB");
        private By name = By.xpath("//p[@id='name']");
        private By header = By.xpath("//h2[@id='header']");

//        public CustomerProfile(WebDriver driver){
//           super(driver);
//        }


        public CAM gotoCAM(){

            //driver.findElement(CAM).click();
            webElement.click(CAM);
            return new CAM();
        }
        public OLB gotoOLB(){
           // driver.findElement(CAM).click();
            webElement.click(OLB);
            return new OLB();
        }
        public String verifyheader(){
            return driver.findElement(header).getText();
        }
        public String verifyName(){
        return driver.findElement(name).getText();
    }
    }

