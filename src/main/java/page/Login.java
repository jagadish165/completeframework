package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login extends Base {
    private By roleselection = By.id("role");
    private By loginlocation = By.id("loc");
    private By submit = By.xpath("//input[@value='Submit']");


public Login selectRole(String role){
    WebElement roleEle = driver.findElement(roleselection);
    select.selectValue(roleEle,role);
    return this;
}
    public Login selectLocation(String location){
        WebElement locationEle = driver.findElement(loginlocation);
        select.selectValue(locationEle,location);

        return this;
    }
    public CustomerSearch submit(){
        //driver.findElement(submit).click();
        webElement.click(submit);
        return new CustomerSearch();
    }

}