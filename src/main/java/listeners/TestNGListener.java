package listeners;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.testng.*;
import page.Base;
import webutil.ConversionUtil;

import java.util.HashMap;

public class TestNGListener implements ITestListener,IInvokedMethodListener {
    long start;
    public static ExtentTest extentTest;
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("test start "+result.getMethod().getMethodName());

        extentTest = Base.extent.extentReports.createTest(result.getParameters()[0].toString());
        //extent.createTest("GeneratedLog")
        String[][] table = ConversionUtil.convertToStringArray((HashMap<String, String>) result.getParameters()[1]);
        extentTest.generateLog(Status.PASS, MarkupHelper.createTable(table, "table-sm"));
     //   extentTest.info(result.getParameters()[1].toString());

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("On test success "+result.getMethod().getMethodName());
        extentTest.pass("Success: ");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("On test fail "+result.getMethod().getMethodName());
        extentTest.fail("Fail: "+result.getMethod().getMethodName());

    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("On test skip "+result.getMethod().getMethodName());
        extentTest.fail("test skip "+result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("On test fail but within success % "+result.getMethod().getMethodName());

    }
    @Override
    public void onStart(ITestContext context) {
        System.out.println("On start "+context.getPassedTests().size());
        System.out.println(System.currentTimeMillis());
         start = System.currentTimeMillis();

    }

    @Override
    public void onFinish(ITestContext context) {

        System.out.println("On finish "+context.getPassedTests().size());
        System.out.println("Time taken in secs " +(System.currentTimeMillis()-start));

    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }


}
