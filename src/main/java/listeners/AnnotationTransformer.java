package listeners;

import datax.TestData;
import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class AnnotationTransformer implements IAnnotationTransformer {
    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        annotation.setRetryAnalyzer(AnnotationTransformer.RetryAnalzyer.class);
        annotation.setDataProviderClass(TestData.class);
        annotation.setDataProvider("testData");
    }

    public class RetryAnalzyer implements IRetryAnalyzer {
        int retry = 0;
        @Override
        public boolean retry(ITestResult result) {
            if (retry < 3) {
                System.out.println("retrying...." + retry);
                retry += 1;
                return true;
            }
            System.out.println("exiting...." + retry);
            return false;
        }
    }
}
