package webutil;

import java.util.HashMap;

public class ConversionUtil {
    public static String[][] convertToStringArray(HashMap<String,String> map){
        String[][] arr = new String[map.size()][2];
        int i=0;
        for(String str:map.keySet()){
            arr[i][0]=str;
            arr[i][1]=map.get(str);
            i++;
        }
        return arr;
    }
}
