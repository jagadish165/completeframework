package webutil;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ExcelUtil {
    public HashMap readTestData(String sheetName) throws IOException, InvalidFormatException {
        File file = new File("src/test/resources/TestData.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        XSSFSheet sheet = workbook.getSheet(sheetName);
        Iterator<Row> rowIterator = sheet.rowIterator();
        HashMap<String, String> data;
        HashMap<String, HashMap<String, String>> dataMap = new HashMap<>();
        List<String> headers = new ArrayList<>();
        String value = "";

        while (rowIterator.hasNext()) {
            String tcID = "";
           data = new HashMap<>();
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {

                Cell cell = cellIterator.next();
                CellType cellType = cell.getCellType();
                if (cellType == CellType.FORMULA) {
                    value = cell.getCellFormula();
                    cellType = evaluator.evaluateFormulaCell(cell);
                }
                switch (cellType) {
                    case NUMERIC:
                        value = Double.toString(cell.getNumericCellValue());
                        break;
                    case STRING:
                        value = cell.getStringCellValue();
                        break;
                    case BOOLEAN:
                        value = Boolean.toString(cell.getBooleanCellValue());
                        break;
                    case BLANK:
                    case _NONE:
                    default:
                        value = "";
                }
                if (cell.getRowIndex() == 0) {
                    headers.add(cell.getColumnIndex(), value);
                } else {
                    if (cell.getColumnIndex() == 0) {
                        tcID = value;
                    } else {
                        try {
                            data.put(headers.get(cell.getColumnIndex()), value);
                        }catch (Exception e){
                            System.out.println("Error while retrieving value for row:"+ cell.getRowIndex()+" col:"+cell.getColumnIndex());
                        }
                    }
                }

            }
            if(tcID!="") {
                dataMap.put(tcID, data);
            }
        }
        return dataMap;
    }
}
