package webutil;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.model.Media;
import listeners.TestNGListener;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class ScreenshotUtil {
     public void takeScreenshot(WebDriver driver) {
         File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
         LocalDateTime localDateTime = LocalDateTime.now();
         String imageName = localDateTime.toString().replaceAll(":","-")+".png";
         String location = "./target/extent-reports/images/"+imageName;

         File screenshotLocation = new File(location);

         try {
             FileUtils.copyFile(screenshot, screenshotLocation);
         } catch (IOException e) {
             e.printStackTrace();
         }
         TestNGListener.extentTest.info(MediaEntityBuilder.createScreenCaptureFromPath("./images/"+imageName).build());
     }

    public void takeElementScreenshot(WebDriver driver, WebElement ele) {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        BufferedImage fullImg = null;
        try {
            fullImg = ImageIO.read(screenshot);


        // Get the location of element on the page
        Point point = ele.getLocation();

        // Get width and height of the element
        int eleWidth = ele.getSize().getWidth();
        int eleHeight = ele.getSize().getHeight();

        // Crop the entire page screenshot to get only element screenshot
        BufferedImage eleScreenshot = fullImg.getSubimage(point.getX()-10, point.getY()-10,
                eleWidth+10, eleHeight+10);

            ImageIO.write(eleScreenshot, "png", screenshot);


        LocalDateTime localDateTime = LocalDateTime.now();
        File screenshotLocation = new File("./src/target/extent-reports/"+localDateTime.toString().replaceAll(":","-")+".png");
        FileUtils.copyFile(screenshot, screenshotLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
