package report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import java.io.File;
import java.io.IOException;

public class ExtentReport {
    public ExtentReports extentReports;
    public static ExtentReport extentReport;
    private ExtentReport(){

        ExtentSparkReporter sparkReporter = new ExtentSparkReporter("target/extent-reports/report.html");
        try {
            sparkReporter.loadXMLConfig(new File("src/main/java/config/spark-config.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        extentReports = new ExtentReports();
        extentReports.attachReporter(sparkReporter);
    }
    public static ExtentReport getInstance(){
        if(extentReport ==null){
            extentReport = new ExtentReport();
        }
        return extentReport;
    }
}
