package wrapper;


import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.model.Media;
import com.aventstack.extentreports.observer.entity.MediaEntity;
import listeners.TestNGListener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import page.Base;
import webutil.ScreenshotUtil;

import java.io.IOException;

public class CustomWebElement  {
    WebDriver driver;
    //By by;
    ScreenshotUtil screenshotUtil;
    public CustomWebElement(WebDriver driver){
        this.driver = driver;
        //this.by = by;
        this.screenshotUtil = new ScreenshotUtil();
    }

    public void click(By by) {
        System.out.println("clicking on element "+by.toString());
        Base.logger.info("clicking on element "+by.toString());
        TestNGListener.extentTest.info("clicking on element");
        screenshotUtil.takeScreenshot(driver);
        driver.findElement(by).click();
        System.out.println("clicked on element "+by.toString());
        Base.logger.info("clicked on element "+by.toString());
    }


    public void submit(By by)  {
        System.out.println("submitting element");

        screenshotUtil.takeScreenshot(driver);
        driver.findElement(by).submit();
        System.out.println("submitted element");

    }


    public void sendKeys(By by,CharSequence... keysToSend) {
        System.out.println("entering text "+keysToSend);
        Base.logger.info("entering text "+keysToSend);
        TestNGListener.extentTest.info("entering text");
        screenshotUtil.takeScreenshot(driver);
        driver.findElement(by).sendKeys(keysToSend);
        System.out.println("entered text "+keysToSend);
        Base.logger.info("entered text "+keysToSend);
    }


    public boolean isSelected(By by) {
        return driver.findElement(by).isSelected();
    }


    public boolean isEnabled(By by) {

        return driver.findElement(by).isEnabled();
    }

    public String getText(By by) {
        return driver.findElement(by).getText();
    }


    public boolean isDisplayed(By by) {
        return driver.findElement(by).isDisplayed();
    }
}

