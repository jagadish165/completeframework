package wrapper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import page.Base;
import webutil.ScreenshotUtil;

import java.io.IOException;

public class CustomSelect {
    ScreenshotUtil screenshotUtil;
    WebDriver driver;
    public CustomSelect(WebDriver driver){
        this.screenshotUtil = new ScreenshotUtil();
        this.driver = driver;
    }
    public void selectValue(WebElement element, String value)  {
        Base.logger.info("selecting value from dropdown "+value);
        screenshotUtil.takeScreenshot(driver);
        Select select = new Select(element);

        select.selectByVisibleText(value);
        Base.logger.info("selected value from dropdown "+value);
    }
}
