package wrapper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;

public class CustomWebDriver {
    public static final ThreadLocal<WebDriver> threadLocal = new ThreadLocal<>(){
        @Override
        protected WebDriver initialValue() {
            return super.initialValue();
        }

        @Override
        public WebDriver get() {
            return super.get();
        }

        @Override
        public void set(WebDriver value) {

            super.set(value);
        }

        @Override
        public void remove() {
            super.remove();
        }
    };




    public WebDriver startDriver(String browser) throws IOException {
        if(browser.equalsIgnoreCase("Chrome")){
            System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("—disk-cache-size=0");
//            options.addArguments("--headless");
//            options.addArguments("--disable-gpu");
//            options.setHeadless(true);
            threadLocal.set(new ChromeDriver(options));
        }else if(browser.equals("Firefox")){
            threadLocal.set(new FirefoxDriver());
        }
        return threadLocal.get();

    }


}
