package datax;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.DataProvider;
import webutil.ExcelUtil;

import javax.naming.event.ObjectChangeListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class TestData {
    ExcelUtil excelUtil = new ExcelUtil();
//    QueryGeneration(){
//         excelUtil = ;
//    }
    @DataProvider
    public Object[][] testData(Method method) throws IOException, InvalidFormatException {
        HashMap<String,HashMap<String,String>> data = excelUtil.readTestData(method.getDeclaringClass().getSimpleName());
        Object[][] obj = new Object[data.size()][2];
        Iterator<String> tcIDs = data.keySet().iterator();
        int i=0;
        while(tcIDs.hasNext()){
            obj[i][0]=tcIDs.next();
            i++;
        }
      for( i=0;i<data.size();i++){
          obj[i][1]=data.get(obj[i][0]);
        }
        return obj;
    }

}
