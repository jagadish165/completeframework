package ui;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.CustomerProfile;

import java.util.HashMap;

public class CustomerProfileTest extends BaseTest{
    ReusableTest reusableTest = new ReusableTest();
    CustomerProfile customerProfile = new CustomerProfile();
    @Test(priority = 1)
    public void VerifyCustomerProfile(String testCase, HashMap data){
        reusableTest.loginwithRoleAndLocation(testCase,data);
        Assert.assertEquals(data.get("CustomerName"),reusableTest.searchCustomer(testCase,data).verifyName());
    }
}
