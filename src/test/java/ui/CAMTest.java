package ui;

import org.testng.annotations.Test;
import page.CustomerProfile;
import page.Base;

import java.util.HashMap;

public class CAMTest extends BaseTest {
    ReusableTest reusableTest = new ReusableTest();
    CustomerProfile customerProfile = new CustomerProfile();

    @Test(priority = 1)
    public void verifyCAM(String testCase, HashMap<String,String> data) {
        reusableTest.loginwithRoleAndLocation(testCase,data);
        reusableTest.searchCustomer(testCase,data);
        customerProfile.gotoCAM().selectProof(data.get("Proof")).selectCountry(data.get("Country"));
    }
    @Test
    public void test(String testCase, HashMap<String,String> data){
        System.out.println(Base.properties.getProperty("env"));
        System.out.println(Base.properties.getProperty("url"));
    }
}
