package ui;

import org.testng.annotations.*;
import page.Base;
import report.ExtentReport;

import java.io.IOException;

public class BaseTest {
    Base base = new Base();
    public static ExtentReport extent;
    @BeforeSuite
    public void beforeSuiteSetup() throws IOException {
        base.killDrivers();
        base.initiateReport();
    }

@BeforeMethod
    public void initialize() throws IOException {
     base.setup();
}
@AfterMethod
    public void wrapup(){
        base.tearDown();
}
    @AfterSuite
    public void close() throws IOException {
        base.killDrivers();
        base.closeReport();
    }
}

