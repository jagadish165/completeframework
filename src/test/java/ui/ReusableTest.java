package ui;

import page.CustomerProfile;
import page.CustomerSearch;
import page.Login;

import java.util.HashMap;

public class ReusableTest extends BaseTest {
    Login login;
    CustomerSearch customerSearch;
    CustomerProfile customerProfile;
    ReusableTest() {
         login = new Login();
         customerSearch = new CustomerSearch();
         customerProfile = new CustomerProfile();
    }
    public CustomerSearch loginwithRoleAndLocation(String testcase, HashMap<String,String> data){
        return login.selectRole(data.get("Role")).selectLocation(data.get("Location")).submit();
    }
    public CustomerSearch loginDefault(){
       return login.submit();
    }

    public CustomerProfile searchCustomer(String testcase, HashMap<String,String> data){
        customerSearch.selectSearchType(data.get("SearchType"));
        String type="";
        switch (data.get("SearchType")){
            case "ID":
                type = "PartyID";
                break;
            case "AccNo":
                type = "AccountNo";
                break;
            case "TIN":
                type = "TIN";
                break;
            case "Name":
                type = "Name";
                break;
        }
        return customerSearch.enterValue(data.get(type)).submit();
}


}
