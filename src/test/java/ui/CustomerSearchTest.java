package ui;

import org.testng.Assert;
import org.testng.annotations.Test;
import page.CustomerProfile;

import java.util.HashMap;

public class CustomerSearchTest extends BaseTest {
    ReusableTest reusableTest = new ReusableTest();
    CustomerProfile customerProfile = new CustomerProfile();
    @Test(priority = 1)
    public void VerifyCustomerSearch(String testCase, HashMap data){
        reusableTest.loginwithRoleAndLocation(testCase,data);
        Assert.assertEquals("Customer Profile",reusableTest.searchCustomer(testCase,data).verifyheader());
    }
}
